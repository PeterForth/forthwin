 In the file winconst.dat 5807 different Windows constants
from modules winbase, wingdi, winuser, richedit, winnt, ras, partially winsock

Just connect ~ day \ wincons \ wc.f file to your project
and all Use on health.

You can also connect your own dictionaries.
The library contains a list of dictionaries and therefore looks for all
connected. Connect as follows:

S "~ day \ wincons \ windows.const" ADD-CONST-VOC
S "~ you \ your.const" ADD-CONST-VOC

Note that ADD-CONST-VOC itself adds the path to the fort system to your path.

Small note - NOTFOUND in wc.f does not ignore large \ small
and looking for a constant as it is. If all constants in the * .const file are capitalized
letters, then there is no need to write them in small program
(spf - not borland pascal). But do not forget that in the module ras constant
contains lowercase letters.

Send files with constants (* .f) that are not in winconst.dat - I have them
there turn on. You can of course do it yourself (steal.f, compile.f, but so centralized,
one file with all possible constants is better than many different little ones :)

compile.f � compiles a file with constants.
            Constants can be defined via #define, CONSTANT
            If via #define, then the rest of the line is ignored.
            for example, you can do this: #define WM_PAINT 15 ###### here you can spoil
            It is possible so:
               #define TEST 1
               #define TEST2 TEST
               TEST2 CONSTANT TEST3
               
            Before constant string
            
            BEGIN-CONST
            
            after constants for example
            
            S "windows.const" SAVE-CONST
            
            If the constant is repeated, then repeats are ignored,
            therefore you can collect constant files from ten * .f
            files with different constants, possibly repeating.
            
            
h2f.f - remakes from * .h file to * .f file.
        After the truth, you have to edit the handles a little, but this is nothing.               
        It is ruled so - the attempt to compile fails, but we get the number
        lines where the error is - in 10 minutes, a 300 kilobyte file is corrected.
        
steal.f - ripping constants from foreign files in YZ format. Description along with
          by code

wc.f - the windows.const file loader - contains the NOTFOUND so that
          just plug in and don't worry about anything.   