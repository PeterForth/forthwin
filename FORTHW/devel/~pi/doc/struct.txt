 Creating Structures with lib \ ext \ struct.f
 
The library facilitates the creation of structures. 
The created structure contains only
The size of the data being declared when the structure is declared.

Creating a simple structure:

STRUCT: name
length - field_name
...
; STRUCT
Example:
STRUCT: COORD
2 - x
2 - y
; STRUCT
A COORD structure is declared, which has 16 bit fields x and y.

Ad structure:
CREATE name_of_name structure_name :: / SIZE ALLOT
Example:
CREATE coordcon COORD :: / SIZE ALLOT
Create a coordcon structure using the COORD pattern.
Getting the address of the structure:
coordcon
The name returns the address of the data structure
Getting the address of the structure field
structure_name :: structure_field
Example:
coordcon COORD :: x
We obtain the address of the field of the declared structure, 
which can be used when reading the structure.
Writing data to structure fields:
13 coordcon COORD :: x W!
20 coordcon COORD :: y W!
Write 13 in the 16 bit field of the structure. coordcon declared structure, 
COORD :: xfield of structure W! write to 16 bit cell fields.
Reading data from structure fields:
coordcon COORD :: x W @
coordcon COORD :: y W @
coordcon declared structure, COORD :: x field of structure W @ read from 16 bit cell
fields.
Determining the size of the structure:
structure_name :: / SIZE
Example:
COORD :: / SIZE
This example returns the size of the structure in bytes.
Creating a complex structure:
STRUCT: CONSOLE_SCREEN_BUFFER_INFO
COORD :: / SIZE - dwSize
COORD :: / SIZE - dwCursorPosition
2 - wAttributes
COORD :: / SIZE - srWindow1
COORD :: / SIZE - srWindow2
COORD :: / SIZE - dwMaximumWindowSize
; STRUCT

CREATE con CONSOLE_SCREEN_BUFFER_INFO :: / SIZE ALLOT
13 con CONSOLE_SCREEN_BUFFER_INFO :: dwSize!
con CONSOLE_SCREEN_BUFFER_INFO :: dwSize @
Create a complex structure, allocate space for the structure, write 13 in the dwSize field and
we also get data from this field.
Writing data to fields that are inherited from another structure:
con CONSOLE_SCREEN_BUFFER_INFO :: dwCursorPosition
COORD :: x 13 SWAP W!

Find the address of the structure field and at this address also find the address of the field
inherited structure and write 13 at this address.
Reading data from fields that are inherited from another structure:
con CONSOLE_SCREEN_BUFFER_INFO :: dwCursorPosition
COORD :: x W @

We find the address of the structure field and at this address we also find the address 
of the polarized structure and we get a 16-bit number from this address.