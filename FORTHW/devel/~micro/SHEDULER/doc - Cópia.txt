
This program is designed to perform certain actions.
at some time or at specified intervals.

The program is controlled by the sheduler.cfg configuration file located in
one directory with the program.

Syntax:
\ after the slash comes the comment to the end of the line
do
  <actions>
  \ these actions will be performed at the specified time
every day | hour | min [at [<hour> hour] [<minutes> min]]
\ or
do
  <actions>
  \ these actions will be performed after a specified period of time
thru <number> [ms | sec | min] \ in the absence of a unit of measurement -
\ milliseconds

After the do block, there are several reaching every / thru commands, each for 
a separate line.

Actions.

This program compiled two separate autopush projects for
pressing windows buttons and deleter to clear directories. Have
each of the bottom has its own set of commands.

Autopush

<hwnd> push - click on the button
  To find the button handler, you can use the commands:
desktop - gives the desktop handler
<hwnd> -> "<string>" - find the window handle (or button) of the child from the <hwnd>
and with the title <string>
So the command looks like this:
desktop -> "Window Title" -> "Child Window (if any)" -> "push" button

Deleter

del <directory with a slash>
\ Delete all files from directory

arch <directory with a slash> <file>
\ Save all files from directory to file