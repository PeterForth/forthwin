A word about Forte. Andrey Tcherezov ac@forth.org
Attempt to formalize the essence of the language :) 07/29/1999

1. Fort system

The traditional fort system consists of two parts: the execution environment (its
often called VFM (virtual fort-machine) and translator. Translator
is engaged in the absorption of the source code of programs (and something at the same time 
produces, what later), VFM is engaged in the execution of program code.

However, the two parts can work independently. VFM without translator can
used to execute a previously compiled program. If this
the program itself does not transmit anything during execution, the translator
she does not need and may be absent. Many applications on Fort
can belong to this class - used without the Fort translator,
only with VFM.

A translator without a native VFM is used, for example, for a targeted compilation.
- creating a program for another platform - when the target VFM is simply not
can work on instrumental vfm. In this case, the target compiler
(extension of the standard Fort translator) simply creates code for the target
platform, it will be done by another VFM on another computer.


2. VFM

For some reason, it is considered that the distinctive features of a virtual machine 
Fort - the presence of two stacks, sewn code and address interpreter. At the very
The first is a convenient implementation of the method adopted in Forte. 
parameter transfer, the second and third - just a special case of the method
code generation and its execution. The fort can compile normal engine
code that does not require address interpretation can compile bytecode,
requiring interpretation, but not targeted, etc., and at the same time continue
remain a fort.

Absolutely the vast majority of programming languages ??use
stack for passing parameters and for storing return addresses from 
subprograms (functions, procedures). Most do it in an implicit way.
- the programmer does not operate with the concept of "stack". But their performing environments are
almost always use a stack. This is a very convenient design for
storing return points when calling multiple nested routines,
for passing parameters and for storing temporary local variables. 

But most languages ??lack one stack. Where did the two stacks come from
Forte? The fort is representative of a fairly small procedural class.
languages. Most languages ??operate with functions � sort of routines,
returning a single value as a result. Return a few
values ??of a function can only either write them to variables whose addresses
passed as parameters, or returning as a result
a pointer to a structure containing several values. When leaving
functions, the data on the stack used by this function can
be safely removed from the stack. And the return address too. On the stack either
Nothing is returned (usually the result of the function is stored when returning
in the processor register), or returns a known number of elements -
one item. The Fort procedure can use similar methods, but
may return multiple values ??on the same stack on which 
parameters were passed (this is a plus, not a minus, but a plus later). And if
when calling the procedure, push the return address to the stack above the parameters
procedures (as done in functional languages), you will have to take
special measures to extract this address from under return values
and also "rally" the data on the stack to eliminate the "hole" from the address
return. You can think of different ways to solve this problem, but
Someone once solved this problem by mixing data and addresses
returns on one stack just do not create - and so there were two stacks:
one for procedure parameters and return values, the other for addresses
return. It is convenient and quite effective, therefore it is widely used.
in the forte. 

However, the Fort could exist with one stack and three, it�s
for the language does not matter. Unfortunately, the practice has been established in the Forte
using the second stack (the return stack) is not only direct 
assignment automatically when calls / returns from procedures, but also explicit
manual manipulations with this stack for storing nameless local and 
temporary variables and even for manual intervention in the return flow
from procedures (which violates the principles of structured programming). WITH
the other hand, the fort does not force it, so to fight
with this phenomenon is not necessary. I was just trying to show that having two
Stacks are not an integral part of the Fort and its hallmark.

What, then, is the hallmark of the Fort virtual machine?
In my opinion, it has no distinctive features, except for the fact that the Fort 
uses not functions, but procedures. Internal structures are stacks,
ways to compile and execute code, etc. - depend on the specific
implementation. Any other special features, such as "universal listification"
Lisp or "universal objectification" Smalltalka with automatic collectors
there is no garbage in the fort machine. The fort machine is very close to conventional processors.
and therefore extremely simple to implement.


3. Translator

Language translator is a native speaker. A programmer writes on Forte,
the Fort translator understands this letter and explains it to the machine in its language,
translates (translator = translator). The translator is the essence of the language.
The translator perceives the program in accordance with the syntactic
rules of the language. In Forte, these rules are simplified to the "physical limit"
simplifications. The syntax is - word1 word2 word3, etc. Words (names
procedures to be executed) are separated by spaces. If the word is
not the name of the procedure, it is trying to identify itself as a numeric 
literal if fails - error. Everything, no more rules in the Fort
not. The translator does not understand anything except words separated by spaces.

All syntax constructs offered by standard Fort are
like the definition of procedures and variables, control statements, etc. -
are not processed by the Fort translator, but by the words themselves,
which can temporarily �take away� from the translator control over the input
stream (text of the program being translated), as well as switch the translator
between the two modes of his work. Mode of operation sets what will do
translator with the found word - immediately execute it (mode 
interpretation) or postpone its execution for later (compilation mode).

The concept of "keyword" in the Fort habitual for traditional languages
is missing. All words are equal with one exception - there are words
"with a sign of immediate execution". These are the words that run
to execute even if the current translator mode is a compilation. Without
such words would be impossible to manage the compilation, including 
return to interpretation mode. In some Forts even the termination
the broadcast is not made upon reaching the end of the input stream,
and upon receipt at the end of the flow words with zero length. This word
they have in the dictionary, has a sign of immediate execution and when 
The execution of this word interrupts the broadcast.

Standard-recommended word sets are simply standardization.
established traditions. The translator that implements the cycle "take the word, find in
dictionary, execute or compile it, take the next word, etc. "
is, in my opinion, the Fort translator is simply on this basis,
even if the dictionary of this translator is empty. Although without a word, he certainly
will be simply "Fort useless translator". Word sets have changed a lot
throughout Fort Fortyne's thirty year history, but this main cycle 
translator - no.


4. Horror stories for beginners

Beginners to study the Fort are often intimidated by the stack and "reverse Polish writing."
We'll see where the forts came from in the forte stack and write back, are they needed,
and whether they are scary.

EXAMPLES:
1) If the word procedure has no input parameters, the program entry
on Forte looks like this:

WORD 1 WORD 2 WORD 3
(linear sequence of commands)

The same on C would look like this: 

FUNCTION1 (); FUNCTION2 (); FUNCTION3 ();
(linear sequence of commands, only with an excess of punctuation)

2) If WORD3 requires two parameters for operation, 
calculated by the words SLOVO1 and SLOVO2, each of which parameters are not
requires, then recording the program on Forte looks like this:

WORD 1 WORD 2 WORD 3
(linear sequence of commands, you can perform one after another)

The same on C would look like this:

FUNCTION3 (FUNCTION1 (), FUNCTION2 ());
(prefix notation: first, the name of the operation is recorded, then its operands;
before performing the FUNCTION3 operation, you need to run ahead and 
FUNCTION1 and FUNCTION2.)

3) Take a concrete example, add 2 and 3.
At the Forte:
2 3 +
(linear sequence of commands: took 2, took 3, folded)

In C:
2 + 3
(infix notation: operation name between operands, you need to run ahead
for the second operand, since at the time of receipt of the "+" troika not yet).

4) Another specific example is to increase the value of the variable N by 1.
At the Forte:
N 1+!
(linear sequence of commands: took a variable and incremented,
"1+!" - this is one command, as ridiculous in writing as ++ in C)

In C:
N ++
(just like on Forte)

5) Another example is to increase the value of the variable N by 5.
At the Forte:
N @ 5 + N!
(linear sequence of commands: took the variable (N), extracted it
value (@), took 5, added to the value of the variable (+), took 
variable (N) and recorded there the resulting value (!))

In C:
N = N + 5;
(they took a variable, they took an "=", it is necessary to assign a value, what value ?,
it is not clear yet, put off N and "=", let's go further - again N, then take "+",
we need to add, one operand is ready, and where is the second? we go
further and we get 5. The offer is over, we go through deferred operations and 
operands in reverse order: add 5 to N and write the result in N).


Question 1: Where are the stacks and writebacks here? The stack is what? Structure in
which "last entered, first come out", i.e. like a machine shop
Kalashnikov, a device for postponing something for later and getting out
in reverse order. Where are we in our examples postponed operands and
operations and performed them then in reverse order? Right in example 5
C program. It is the stack used by the C translator to
unfolding records of the form N = N + 5 in a sequence of machine commands
"take address N, extract value, take 5, add them, take address N
and write the sum there ", since only such a linear sequence 
commands can execute the processor. Those. programmer meaning this
sequence of commands, writes it back to C on C, then
translator C, expands it into a direct sequence of commands and
in this form, it can be performed by the processor. Stack and writeback
on the face. In Forte, in all examples the sequence is the same -
direct, no rush ahead to calculate the missing operands.
Each word already works with the previously calculated operands. And exactly how
temporary storage of previously calculated operands and used by the stack in
Forte. In Example 3, two operands were stored on the Fort stack - 2 and 3, and the word
"+" took them from there. In the same C example when calculating on the stack
there are not only deferred operands, but also deferred operation.
Anyway - the result is the same - linear machine code. With
the execution of this code, the stack may be present, and may be absent
in both languages. For example, if operands in floating point C, then
both of them will run on the x87 coprocessor stack and operations
on them will be produced there exactly as on Fort operands
on the internal stack of the Fort processor or software FVM. It is dependent
from the implementation of minor details. Stacks take place in all
modern programming languages. The fort may be more stackable.
only due to the fact that he has a lot of operations to manipulate
this stack explicitly, while in other languages ??this stack is for
programmer is only available as a container for parameters and local 
variables, with the ability to refer to them only by name. In the forte
You can also use named local variables, but you can
do without them.

Question 2: Which recording system is simpler: C, or Fort? If a
not sure, look again for examples. In C, we have a mixture of prefix,
infix and postfix notation. In addition, do not do without brackets and
expression separators ";". Brackets group operands and operations � i.e.
They say the translator "I do not want to perform operations in order, but I want
back to back and mixed. A semicolon tells the translator what it is time
stop running forward and it's time to go through the stack of pending operations
and operands back to expand the "reverse American record" in
linear sequence of commands for computers Fon-Neymanovskoy
architecture. In the Forte in all examples the same primitive order
records - a direct sequence of actions. Brackets are not needed there -
if you need another sequence of command execution, then in Forte
this can be written in exactly this other sequence. Brackets in
Forte is used for commenting (as in this sentence of the Russian
language). And the limiter of sentences is not needed, because no need to come back
backwards At each moment in time everything that was "before" is already done and
there is no reason to return. Semicolon is used in Forte only
the end of the definition of the procedure (by the way, this is in vain, you can also not limit
but more on that later).

The Fort's recording system may seem complicated only at first. 
superficial glance. In fact, you have already learned the syntax of the Fort -
words and spaces and nothing else. And remember how many years you have studied
algebraic form of the record on which the C language is based? Fort -
syntactically the simplest language. And if somewhere there are difficulties,
it is not a matter of Fort ideology, but the realization of concrete words. Set
The words of the standard Fort are indeed very unusual and chaotic. Cause
- chaotic development for 30 years. Non-optimal linguistic
constructs are a disease of most languages, especially such as
Fort. But the Fort is flexible, and you can not use those of his means
which you do not like. Moreover, you can CREATE those funds.
languages ??that you see fit - examples will be in this article.

I guess I did not remove the fears of newcomers, but only aggravated them. But I do not
promised to relieve fear. But as for the stack and syntax -
hopefully dotted i. Stacks are everywhere, and Fort syntax
straighter all straight lines. If in Polish this is a writeback, then this is a problem.
Poles or the one who came up with this name. And in our Forte everything is straight.


5. Language features

You can often be a witness to the dispute "in the A language there is an opportunity to make
so, and in language B so not to do. "From this it is concluded that the language" A " 
it is better. A more correct conclusion is the A language, which is better when it is necessary to "do so."
If in language �B� there is no possibility to do �so� by ordinary means,
and there is no possibility to expand the language "B" "by such" means, then the language "A"
definitely better than language "B" for "such" applications. If "B" is a language
extensible, then �A� is better as long as it is possible to expand �B�
necessary means. Although "B" can become better than "A" in "such"
applications, if, after expansion by the necessary means, these means
turned out to be more convenient. Those. the conclusion can be unambiguous only if one
of the compared languages, we do not expand in such a direction.

In fact, programmers find that in most languages
most tasks can be solved, and the choice of a specific language is determined
many additional factors, not only on the basis of "may-not-can."
Often especially prominent language features in a particular field 
(for example, 100% portability) leads to flaws in other
(for example, for portability reject efficiency). Language
best of all suitable for all existing tasks, does not exist. but
extensible languages ??have advantages - you can fill in the gaps
opportunities.

You can expand the language in different ways. The first way is to write
library in this language itself to implement any new 
opportunities. That is just a set of additional features included
into the program. The second expansion method is with external modules that can
be written in another language. For example, the PHP language is extended by modules
written in C language. Not only because such modules are faster.
work, but mainly due to the fact that PHP lacks direct
operating system interfaces and low-level tools. The main
way to guarantee portability. Need any missing
means - they write external modules in C, which are on different platforms
different, but the PHP language interface with them will be made the same everywhere.
In this case, PHP is extensible, and C is extender. Such a scheme
extensions adopted in the vast majority of modern languages. Because
C has special privileges in modern operating systems: they themselves
written in C and therefore better adapted to it by software 
interfaces. The C language was designed to develop operating systems
therefore, it has enough low-level tools. All this his role
"universal expander" and explained. Translators and Virtual Machines
Many languages ??are also written in C language. The fort is almost the same age as the C language.
(older by 1-2 years) and is the same low-level. Up to availability
built-in assembly language. It can expand itself, although for
communication with the operating system is forced to use connection agreements,
adopted in C - call functions of the operating system as functions of the C language.

For Fort, extensibility is a key opportunity. Standard Fort includes
basically only those tools that were used to create it
the translator, and since the translator is primitive, then the available means
the same level: arithmetic operations, memory operations (including
lines), progress control operations, and file I / O. 
In C and Pascal, the set is the same. In the Forte there are still ready means of reference.
command dialogue in the console, since the translator can work in
interpreter (as Lisp, BASIC, Smalltalk, Postscript, SQL, Perl, etc.)
Everything else is extensions. The presence or absence of the necessary
the extension programmer is determined by the value for him specific 
Fort system. But the value of the Fort itself is simply its ability.
extensions. The fort is one of the few languages ??whose translators are written
on him. Although FVM is often written not only on Forte, but also on
assembly language or C. There are even implementations of FVM in Java.

A consequence of the primitiveness of the translator in Forte is that the syntactic
and language control constructs are not translator keywords,
and procedures. Those. they are not just "markers" in the source text, but
effective components that are no different from other procedures. And once
so, not only the set of available functions is extended, as in
C and most other languages, but also a set of broadcast management tools.

The syntactic and control constructs of a language are the means of determining
new procedures, variables, conditional if statements, while loop operators,
do, etc. All this is available in Forte for replacement or expansion. Replace
the if statement is hardly necessary for someone in normal programming, but with the target
compilations to another platform in Forte replace this operator so that
he compiled the code for the target platform, besides the built-in Fort
assemblers also often make if commands, which are macros for
assembler. And adding design is not superfluous. For example, in the Forte originally
there are no object-oriented programming tools, but their
can add to the Fort any Fort programmer, not just the developer
translator. And any object model, which only can come up with.


6. Subtotals

The simplest syntax and the corresponding simplest translator are 
The main idea of ??the Fort. Total extensibility is a major opportunity.
Fort. This possibility is a consequence of the implementation of such an idea.
Stacks, dictionaries, FVM and other implementation details are the consequences of such 
approach.


7. The Place of the Fort in the Genealogy of Languages

The Fort, as far as I know, has no direct ancestor. But there is a close
senior relative - Lisp. Perhaps the author of the Fort never programmed
on Lisp, and maybe not even aware of its existence, but the ideas are the basis
the same language is used - extreme simplicity. At first glance, languages ??are quite
not alike. Syntactically - Lisp programs contain more brackets than
programs in any other language, in Forte brackets only in comments.
The core of Lisp is lists everywhere, the Forth lists only in dictionaries. But Lisp
almost the same simple translator is just one way to record. And the same
extensibility. The ideas are the same, but very different ways of implementation.
Lists are an integral part of the Lisp ideology, its mathematical
and physical basis. The fort, alas, does not shine with mathematical education.

And the Fort has a direct descendant - PostScript. He inherited it
best features include easy syntax and extensibility. This is practically a fort,
but with a different set of words, much more logically designed. but
the developers deliberately did not make a universal language from PostScript.
They needed a language that would draw an image of a page in printers. And, as you know,
They coped with this task perfectly. Low-level language tools
There is no postscript, so it expands only in the "high" direction - you can
define new graphical and computational procedures based on 
available.

And relatively recently appeared at the Fort and grandson - the language of PDF (Portable
Document Format), the heir to PostScript. It looks like PDF developers about
Forte did not hear, because of some useful properties have forgotten. Tongue
turned out to be quite complicated, only partially compatible with PostScript
(there are PostScript scripts in PDF written in PostScript!), but
Thanks to Adobe�s marketing efforts, the PDF interpreter is almost on
each computer in the form of AcrobatReader.

There is another Fort-like language - this is DSSP. It was developed in Russia and
actively developing. The authors conducted a major overhaul of fort ideas and
have put forward their own unique principle for DSSP: one word program 
matches one word of code.

Other close relatives of the Fort probably do exist, but they are unknown to me.


8. Why did I write all this

All these notes are not an introduction to the Fort, are not bait or
horror stories for beginners in the Fort, and the old forters can cause 
irritation and allergies. What is the purpose of the presentation?

I described the current state of affairs around the Fort without going into details.
Considered the most common misconceptions. And stopped in some detail
on the fact that in my opinion is the main value of the Fort - the simplest
syntax (and all that follows from this). What for?

Then, in order to put your thoughts in order first of all, and at the same time give
you justify some of my technical solutions when creating 
Fort's broadcasters, and not to shock the old forters
reforming the Fort when implementing SPF4 (who do not know what
SPF, that is optional :). I'm not going to spoil anything in the Fort, I
I still want to simplify it - remove the "shamanism" inherent in programming
on Forte, remove garbage from the base dictionary, but keep compatibility
by implementing ANS FORTH 94 as an extension.

This is what we will do now.


9. Work plan

The minimum task is to write the minimum Fort translator, whose funds 
will be enough to compile yourself. "Just minimal
Fort translator is a much simpler task, and we will begin with it.
And then we will see. Suddenly I have, as often happens, just not enough
time to finish writing SPF4 to the end, and in the next run there will be other ideas
As with SPF4, it was already five times ...;)

We will, of course, be writing Fort.


10. The main translator cycle

: Eval (-)
  BEGIN
    NextWord? DUP
  WHILE
    TranslateWord
  REPEAT DROP
;

I hope that even non-forts here everything is clear. Cycle take the word
The "broadcast word" continues until the end of the input stream is reached.
NextWord and TranslateWord words are missing in the standard Forte, but all
Forters know how to implement them with standard words. However we
temporarily forget about this knowledge.

I will write non-standard words using lowercase letters,
standard - only uppercase.

The lack of parameters in the implementation of Eval is due to the presence 
The concepts of "current input stream" and "current broadcast context."

The word NextWord returns the address of the string and the length of the string. If achieved
the end of the input stream is the length of string 0 and the WHILE loop ends.

If errors occur along the way - an exception is thrown (THROW), 
Eval interrupting operation.


11. Word selection (parser)

It would be easy to read the entire Fort source file in
memory, as does the C-translator, but in Forte there are circumstances that
forced to treat the text of the program not as a continuous flow, but as 
broken into chunks. In Forte there is no concept of "chunk", but we will
to use as the designation of the portion of the program text received for one
read operation of the input stream. The fort, unlike C, can work in
dialogue mode with the user (programmer). In this mode, the programmer
expects the translator to process its input line by line, and immediately
after entering the line to give the result, without waiting for the end of the input stream.
In addition, the Fort can traditionally use as an input stream
"blocks" - fragments of the program with a size of 1 kilobyte. It is used for
transfer programs between computers of different types with different file
systems and in general without file system and operating system. 

Reading the source text in small portions is convenient because it requires
less memory for broadcast. The fort is famous for the fact that its translator can
work on minimal, including embedded, platforms where 
The fort system may have only a few kilobytes of memory available. We are this
the application will also not be excluded.

The input stream for the Fort translator can be the program text, 
coming from any source - blocks, files, keyboards, sockets,
GUI, etc. For the word Eval, it doesn�t matter, it has to do with
read in memory chunk. Attributes of this chunk: start address, length
and the current word pointer is offset from the start of the chunk. In the forte
the address of the beginning and the length returns the word SOURCE, and the offset is in
variable> IN.

: NextWord (- c-addr u)
  SkipDelimiters ParseWord
;

SkipDelimiters skips spaces (or "whitespace", to which
may include TAB, CR, LF, etc.) at the current position> IN, before the first
non-whitespace character, and sets> IN to it.

: SkipDelimiters (-) \ skip white space characters
  BEGIN
    OnDelimiter
  WHILE
    > IN 1+!
  REPEAT
;

: OnDelimiter (- flag)
  GetChar SWAP IsDelimiter AND
;

: GetChar (- char flag)
  EndOfChunk
  IF 0 FALSE
  ELSE PeekChar TRUE THEN
;

: EndOfChunk (- flag)
  > IN @ SOURCE NIP <0 = \> IN is not less than the length of the chunk
;

: CharAddr (- c-addr)
  SOURCE DROP> IN @ +
;

: PeekChar (- char)
  CharAddr C @ \ character from current value> IN
;

: IsDelimiter (char - flag)
  BL 1+ <
;

ParseWord selects non-whitespace characters from the string to the first whitespace.
and returns the address and length of this sequence of characters (words) in
input line.

: ParseWord (- c-addr u)
  CharAddr> IN @
  SkipWord
  > IN @ SWAP -
;

: SkipWord (-) \ skip non-blank characters
  BEGIN
    OnNotDelimiter
  WHILE
    > IN 1+!
  REPEAT
;

: OnNotDelimiter (- flag)
  GetChar SWAP IsDelimiter 0 = AND
;

This is a complete implementation of the Fort parser (NextWord) - 11 simplest procedures
total size of about 50 lines. It can already be translated standard
Fort. See spf4_parser.f from "parser begin" to "parser end".

Word

: TEST (-)
  BEGIN
    NextWord? DUP
  WHILE
    TYPE SPACE
  REPEAT DROP
;

by doing

TEST AAA BBB CCC

will give the result:

AAA BBB CCC

Nevertheless, our implementation of NextWord parser was cumbersome. 
versus Fort�s traditional implementation of the word WORD (standard 
word to extract a word from a chunk). Very often, the WORD is implemented in
Forte in the form of a single procedure in assembler. But adopted here
implementation, we killed a lot of birds with one stone: first, it is much clearer than
assembler; secondly, it can be compiled in any version of the Fort
on any processor; thirdly, we received as many as 10 additional words,
which will be useful to us in other programs - a whole library for 
implementations of other parsers; fourth, our implementation does not use
additional buffers for storing words, which is more convenient in multi-threaded
fort system.

Note: for spf4_parser.f translation on SwiftForth and Win32for
I had to first define the word 1+!
: 1+! DUP @ 1+ SWAP! ;
because this word is absent in these forts.


12. Word search (dictionaries)

For the above cycle, the translator lacks the implementation of the word
TranslateWord, which should search for words in a dictionary and perform
or compile a word. If the search fails, try
use the word as a literal.

Fort procedures - words - are stored in linear lists - dictionaries. The words
when defined are added to the end of the list, and the search starts at the end
list to allow word overrides. Dictionaries may be
several, and the search for the word translator is done in several dictionaries.
The current set of dictionaries to be viewed and the search order is set
in a special stack of dictionaries. This stack is called a context.

The standard Fort word for context search is FIND, however it does not
very convenient, because uses a string with a counter as a parameter,
and the values ??returned to them are not enough for some applications. Word
SEARCH-WORDLIST searches only in one dictionary, and also returns incomplete
result set, so we implement our own search word set.

Fort dictionaries (vocabulary) are actually linked lists of pairs.
key value and are close analogues of associative arrays, 
dictionaries (dictionary), etc. structures available in many other languages
programming. More precisely in almost all languages, because without such a convenient
Structures to manage difficult. Of the well-known languages, dictionaries or their
other names are in Perl, PHP, PostScript, Smalltalk, Lisp, etc.), 
so the widespread belief about the special uniqueness of the Fort dictionaries on 
my view is wrong. Although there are differences in implementation. For example, flags at
vocabulary elements are rarely found in other languages ??(I only know in 
PostScript), on the other hand, these flags can be "emulated" in other 
languages.

According to ANS Forth94, dictionaries are now officially called
WordLIST - a list of words.

The dictionary can be considered as a special case of a simpler structure -
list. Each item in the list has some content and a pointer to
next item. In the case of a dictionary, the contents of the list item will be
a pair of pointers to the name and value of the dictionary element. If the dictionary element
- procedure, then the name is the name of the procedure, and the value is the address of the procedure code
(not necessarily the real address, but what is called execution in the standard
token, i.e. some link that can execute the word EXECUTE).

Let the Where clause look for a list item by its name in the context dictionaries and 
returns the id of the dictionary wid and a pointer to the item in the item list.
By item it will be possible to get the name of the element with the word Name, the value of the element 
the word Value, the flags of the element with the word Flags, and "something else good," 
if we ever need it, it will be easy to add to such an object.

(Where and other vocabulary-specific words 
  see spf4_trans_dep.f)

: Where (addr u - wid item true | addr u false)
  ...
;
: IsImmediate (item - flag)
  ItemFlags & Immediate AND
;
: Interpreting (- flag)
  STATE @ 0 =
;
: AsWord (item - ...)
  DUP IsImmediate Interpreting OR
  IF ExecuteWord ELSE CompileWord THEN
;
: TranslateWord (addr u - ...)
  Where IF AsWord ELSE AsLiteral THEN
;


12.1. The specific implementation of dictionaries

Traditionally, in Fort systems, all dictionaries are physically stored together in 
one continuous chunk of memory. Dictionary entries of different dictionaries when
this can alternate. Some Fort systems may have separate
storage of titles of dictionary entries, procedure bodies and data areas.
We also realize the possibility of separate storage. This will make it more convenient
create temporary dictionaries for local variables, make plugins
binary images of dictionaries and possibly implement various dictionaries for dictionaries
�protocols� of access to word lists and dictionary entry bodies, which will allow
work transparently with dictionaries in other types of storage devices,
than RAM (network, registry, flash, etc.).

In Forte, there is a word for creating unnamed dictionaries - WORDLIST, which
creates an empty dictionary and returns its id. Further
Search in the dictionary by name is performed by the word SEARCH-WORDLIST, which in
if successful, returns the executable token xt and the flag of the presence of the attribute
IMMEDIATE. To create named dictionary entries is the word CREATE,
which takes the name of the word from the input stream and creates a dictionary entry in
current compilation dictionary. Some fort systems have the word HEADER,
used when implementing the word CREATE, but not creating any code
in the dictionary entry. And the word HEADER uses words that explicitly modify
vocabulary. These words are different everywhere (+ WORD in SPF, "HEADER in Win32for,
WID-HEADER in SwiftForth). The principle is the same - for the operation is necessary
word name and dictionary dictionary. The body of the dictionary entry is supposed to build
after creating the title. However, there is a standard word: NONAME for
create unnamed body procedure - returns xt.

We introduce a more "transparent" set of lower-level words, which 
allows you to implement the mentioned Fort words to create words in the dictionary
and to search for them, and at the same time give a set of words for more convenient work with
a dictionary as an associative array with strings as keys.

(error codes are not returned, in case of errors - THROW)

0
CELL - iWid \ pointer to the dictionary where this item is created.
CELL - iLink \ pointer to the previous item
                \ iWid and iLink are redundant, but it will be faster
CELL - iName \ name pointer
CELL - iValue \ pointer to value
CELL - iFlags \ element flags (immediate, executable, etc)
CREATE / Item

: NewValue (- value)
  \ create a new object with no name, no type, in temporary storage
;
: NewItem (wid - item)
  \ create empty dictionary entry
;
: Value! (value item -)
  iValue!
;
: Flags! (flags item -)
  iFlags!
;
: Name! (name item -)
  iName!
;
: InsertValue (value wid - item)
  \ Attach an object to the dictionary, return the resulting dictionary element
  NewItem SWAP OVER SetItemValue
;
: Def (item addr u -)
  \ name the dictionary item
  S> C SWAP Name!
;
: CreateItem (addr u value wid -)
  \ save the value object in the wid dictionary as addr u
  InsertValue def
;
: DeleteItem (item -)
;
: GetItem (addr u wid - item)
;