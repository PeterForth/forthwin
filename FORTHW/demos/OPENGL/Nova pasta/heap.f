 
\ -----------------------------------------------------------------------------
\ __          ___       ____ ___
\ \ \        / (_)     |___ \__ \   heap for Windows
\  \ \  /\  / / _ _ __   __) | ) |  pi@alarmomsk.ru
\   \ \/  \/ / | | '_ \ |__ < / /   ������⥪� ��� ࠡ��� � ��祩
\    \  /\  /  | | | | |___) / /_   Pretorian 2007
\     \/  \/   |_|_| |_|____/____|  v 1.0
\ -----------------------------------------------------------------------------
 
 
 MODULE: _HEAP

WINAPI: HeapLock KERNEL32.DLL
WINAPI: HeapUnlock KERNEL32.DLL
WINAPI: HeapValidate KERNEL32.DLL
WINAPI: HeapCompact KERNEL32.DLL
WINAPI: HeapSize KERNEL32.DLL

EXPORT

\ Get the handle of the calling process heap
: Heap (-> handle)
	GetProcessHeap;

\ Create a bunch (0 - error)
: HeapNew (-> handle)
	0 4096 4 HeapCreate;

\ Delete heap (0 - error)
: HeapDel (handle -> flag)
        HeapDestroy;

\ Squeezes a bunch 
: HeapZip (handle ->)
	1 SWAP HeapCompact DROP;

\ Block heap access for other threads
: HeapLK (handle ->)
	HeapLock DROP;

\ Unblock heap access for other threads
: HeapUL (handle ->)
	HeapUnlock DROP;

\ Check a bunch for errors
: HeapTest (handle -> flag)
	0 1 ROT HeapValidate;

\ Allocates heap memory
: MemNew (n handle -> addr)
	9 SWAP HeapAlloc;

\ Deletes allocated memory from heap (0 - error)
: MemDel (addr handle -> flag) 
	1 SWAP HeapFree;

\ Get the size of the allocated memory
: MemSize (addr handle -> n)
	1 SWAP HeapSize;

\ Resize allocated memory
: MemResize (n addr handle -> addr)
	9 SWAP HeapReAlloc;

\ Check allocated memory for errors
: MemTest (addr handle -> flag)
	1 SWAP HeapValidate;

; MODULE

\ EOF

Heap (-> handle) - get the handle of the calling process heap
HeapNew (-> handle) - create a bunch (0 - error)
HeapDel (handle -> flag) - delete heap (0 - error)
HeapZip (handle ->) - compresses a bunch
HeapLK (handle ->) - block access to the heap for other threads
HeapUL (handle ->) - unblock access to the heap for other threads
HeapTest (handle -> flag) - check the heap for errors (0 - error)
MemNew (n handle -> addr) - allocates memory from the heap
MemDel (addr handle -> flag) - removes allocated memory from the heap.
MemSize (addr handle -> n) - get the size of the allocated memory
MemResize (n addr handle -> addr) - change the size of the allocated memory
MemTest (addr handle -> flag) - check allocated memory for errors (0 - error)