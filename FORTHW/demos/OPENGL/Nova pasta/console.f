  \ -----------------------------------------------------------------------------
\ __          ___       ____ ___
\ \ \        / ( _ )     |___ \__ \   Console for Windows
\  \ \  /\  / / _ _ __   __ ) |  ) |  pi@alarmomsk.ru
\   \ \/  \/ / | | '_ \ |__ < / /   ������⥪� ��� ࠡ��� � ���᮫��
\    \  /\  /  | | | | |___ ) / /_   Pretorian 2007
\     \/  \/   |_|_| |_|____/____|
\ -----------------------------------------------------------------------------
 
MODULE: _CONSOLE

WINAPI: SetConsoleTitleA KERNEL32.DLL
WINAPI: SetConsoleCursorPosition KERNEL32.DLL
WINAPI: SetConsoleTextAttribute KERNEL32.DLL
WINAPI: SetConsoleCursorInfo KERNEL32.DLL
WINAPI: GetConsoleCursorInfo KERNEL32.DLL
WINAPI: SetConsoleScreenBufferSize KERNEL32.DLL
WINAPI: GetConsoleScreenBufferInfo KERNEL32.DLL
WINAPI: FillConsoleOutputCharacterA KERNEL32.DLL
WINAPI: FillConsoleOutputAttribute KERNEL32.DLL
WINAPI: SetConsoleDisplayMode KERNEL32.DLL
WINAPI: WriteConsoleOutputCharacterA KERNEL32.DLL

0 VALUE XWin \ X coordinate of the virtual window
0 VALUE YWin \ Y coordinate of the virtual window
0 VALUE LWin \ virtual window length
0 VALUE HWin \ virtual window height
7 VALUE CWin \ character color
0 VALUE BWin \ character background

EXPORT

\ Duplicate the specified number of numbers on the stack
: DUPS  (   n ->   )
	DUP 0 ?DO DUP PICK SWAP LOOP DROP
 ;

\ Pack coordinates in number
: XY->N (   xy -> n  )
	16 LSHIFT OR;

\ Unpack coordinates from among
: N->XY (   n -> xy  )
	DUP 0xFFFF AND SWAP 16 RSHIFT;

\ Pack color and background in number
: Color-> N ( background color -> n )
	4 LSHIFT OR;

\ Unpack color and background from number
: N->Color ( n -> background color )
	DUP 0xF AND SWAP 4 RSHIFT;
	

\ Change Console Title
: SetTitle ( addr n -> )
	DROP SetConsoleTitleA DROP;

\ Set the cursor in the console to the specified coordinates
: SetLocate ( xy -> )
	XY-> N H-STDOUT SetConsoleCursorPosition DROP;

\ Hide cursor on console
: HideCursore ( -> )
	0 0 SP @ DUP H-STDOUT GetConsoleCursorInfo DROP
	ROT DROP 0 -ROT
	H-STDOUT SetConsoleCursorInfo DROP 2DROP;

\ Show cursor on console
: ShowCursore ( -> )
	0 0 SP @ DUP H-STDOUT GetConsoleCursorInfo DROP
	ROT DROP 1 -ROT
	H-STDOUT SetConsoleCursorInfo DROP 2DROP;

\ Cursor size ( 0-100 )
: SizeCursore ( n -> )
	>R 0 0 SP @ DUP H-STDOUT GetConsoleCursorInfo DROP
	SWAP DROP R> SWAP
	H-STDOUT SetConsoleCursorInfo DROP 2DROP;

\ Size of console ( from 80 ) ( from 25 )
: SizeConsole ( lenght height -> )
	XY-> N H-STDOUT SetConsoleScreenBufferSize DROP;

\ Console length
: GetLength ( -> n )
	0 0 0 0 0 0 SP @ H-STDOUT GetConsoleScreenBufferInfo DROP
	>R 2DROP 2DROP DROP R> 0xFFFF AND;

\ Console Height
: GetHeight ( -> n )
	0 0 0 0 0 0 SP @ H-STDOUT GetConsoleScreenBufferInfo DROP
	>R 2DROP 2DROP DROP R> 16 RSHIFT;

\ Coordinate of the cursor X
: GetX ( -> n )
	0 0 0 0 0 0 SP @ H-STDOUT GetConsoleScreenBufferInfo DROP
	DROP> R 2DROP 2DROP R> 0xFFFF AND;

\ Y coordinate of the cursor
: GetY ( -> n )
	0 0 0 0 0 0 SP @ H-STDOUT GetConsoleScreenBufferInfo DROP
	DROP> R 2DROP 2DROP R> 16 RSHIFT;

\ Get cursor coordinates
: GetLocate ( -> xy )
	GetX GetY;

\ Current console color
: GetColor ( -> n )
	0 0 0 0 0 0 SP @ H-STDOUT GetConsoleScreenBufferInfo DROP
	2DROP> R 2DROP DROP R> 0xF AND;

\ Current console background
: GetBackground ( -> n )
	0 0 0 0 0 0 SP @ H-STDOUT GetConsoleScreenBufferInfo DROP
	2DROP> R 2DROP DROP R> 4 RSHIFT;

\ Change the color of the output characters to the console
: SetColor ( n -> )
	
	GetBackground 4 LSHIFT OR H-STDOUT SetConsoleTextAttribute DROP;

\ Change the background of outgoing characters to the console
: SetBackground ( n -> )
	16 * GetColor + H-STDOUT SetConsoleTextAttribute DROP;

\ Change X coordinate of cursor in console
: SetX ( n -> )
	GetY SetLocate; 

\ Change cursor Y coordinate in console
: SetY ( n -> )
	GetX SWAP SetLocate; 

\ Clear console
: Cls ( -> )
	0 0 GetLocate XY-> N DUP> R 32 H-STDOUT
	FillConsoleOutputCharacterA DROP
	0 0 R> GetColor GetBackground Color-> N H-STDOUT
	FillConsoleOutputAttribute DROP
	0 0 SetLocate;

\ Expand console to full screen
: FullConsole ( -> )
	0 1 H-STDOUT SetConsoleDisplayMode DROP;

\ Return fullscreen console to window
: WindowsConsole ( -> )
	0 0 H-STDOUT SetConsoleDisplayMode DROP;

Set attributes in console window
: AttrWindow ( -> )
	XWin YWin XY-> N
	HWin 0 ?DO
	DUP 0 SWAP LWin CWin BWin Color-> N H-STDOUT
	FillConsoleOutputAttribute DROP 0x10000 +
	LOOP DROP;

\ Clear console window without changing attributes
: ClearWindow ( -> )
	XWin YWin XY-> N
	HWin 0 ?DO
	DUP 0 SWAP LWin 32 H-STDOUT
	FillConsoleOutputCharacterA DROP 0x10000 +
	LOOP DROP;

\ Clear console window
: ClsWindow ( -> )
	ClearWindow AttrWindow;

\ Swap the background colors and colors of the console in places
: SwapColor ( -> )
	GetBackground GetColor SetBackground SetColor;

\ Output a line without shifting the cursor and without changing color
: Print� ( n addr xy -> )
	XY-> N 0 SWAP 2SWAP SWAP H-STDOUT WriteConsoleOutputCharacterA DROP;

\ Output a character without moving the cursor and without changing color
: Emit� ( xy char -> )
	> R XY-> N 0 SWAP 1 RP @ H-STDOUT WriteConsoleOutputCharacterA
	R> 2DROP;

\ Single horizontal line
: LineH ( n -> )
	0 ?DO 0xC4 EMIT  LOOP ;

\ Double horizontal line
: DLineH ( n -> )
	0 ?DO 0xCD EMIT  LOOP ;

\ Single vertical line
: LineV ( n -> )
	0 ?DO GetLocate 0xB3 EMIT 1+ SetLocate  LOOP ;

Double Vertical Line
: DLineV ( n -> )
	0 ?DO GetLocate 0xBA EMIT 1+ SetLocate  LOOP ;

\ Print single frame on virtual window
: Box ( -> )
	GetColor GetBackground CWin SetColor BWin SetBackground
	XWin YWin SetLocate
	0xDA EMIT LWin 2- LineH 0xBF EMIT
	XWin YWin HWin 2- 0 ?DO
	1+ 2DUP SetLocate 0xB3 EMIT LWin 2- SPACES 0xB3 EMIT
	LOOP
	1+ SetLocate 0xC0 EMIT LWin 2- LineH 0xD9 EMIT
	SetBackground SetColor;

\ Display a double frame on the virtual window
: DBox ( -> ) 
	GetColor GetBackground CWin SetColor BWin SetBackground
	XWin YWin SetLocate
	0xC9 EMIT LWin 2- DLineH 0xBB EMIT
	XWin YWin HWin 2- 0 ?DO
	1+ 2DUP SetLocate 0xBA EMIT LWin 2- SPACES 0xBA EMIT
	LOOP
	1+ SetLocate 0xC8 EMIT LWin 2- DLineH 0xBC EMIT
	SetBackground SetColor;

\ Standard console attribute settings
: Console ( -> )
	WindowsConsole
	0 SetBackground
	7 SetColor
	80 25 SizeConsole
	10 SizeCursore
	Showcursore
	Cls;
	

GetLength TO LWin
GetHeight TO HWin
GetColor TO CWin
GetBackground TO BWin

; MODULE
\ EOF

--- Common words ---
DUPS ( n -> ) - duplicates the specified number of numbers on the stack
XY-> N ( xy -> n ) - pack coordinates into a number
N-> XY ( n -> xy ) - unpack the coordinates from
Color-> N ( background color -> n ) - pack color and background into a number
N-> Color ( n -> background color ) - unpack color and background from a number

--- console output ---
Char� ( xy char -> ) - output a character without shifting the cursor and changing
		colors
Print� ( n addr xy -> ) - output the string without moving the cursor and
		color changes
LineH ( n -> ) - single horizontal line
DLineH ( n -> ) - double horizontal line
LineV ( n -> ) - single vertical line
DLineV ( n -> ) - double vertical line
Box ( -> ) - output a single frame ( set by nWin )
DBox ( -> ) - display a double border ( set by nWin )


--- Actions with coordinates ---
SetLocate ( xy -> ) - set the cursor in the console to the specified coordinates
GetLocate ( -> xy ) - get cursor coordinates
SetX ( n -> ) - change the X coordinate of the cursor in the console
SetY ( n -> ) - change the Y coordinate of the cursor in the console
GetX ( -> n ) - the coordinate of the cursor X
GetY ( -> n ) - coordinate of the cursor Y

--- Actions with cursor ---
HideCursore ( -> ) - hide the cursor on the console
ShowCursore ( -> ) - show the cursor on the console
SizeCursore ( n -> ) - cursor size ( 0-100 )

--- Console actions ---
SetTitle ( addr n -> ) - change console title
FullConsole ( -> ) - expand the console to full screen
WindowsConsole ( -> ) - minimize fullscreen console to window
SizeConsole ( lenght height -> ) - console size ( from 80 ) ( from 25 )
GetLength ( -> n ) - the length of the console
GetHeight ( -> n ) - console height
ClearWindow ( -> ) - clear the window without changing attributes ( set by nWin )
Cls ( -> ) - clear console
ClsWindow ( -> ) - clear the console window ( set by nWin )

--- Actions with console colors ---
GetColor ( -> n ) - current console color
GetBackground ( -> n ) - current console background
SetColor ( n -> ) - change the color of output characters to the console
SetBackground ( n -> ) - change the background of outgoing characters to the console
SwapColor ( -> ) - change the background colors and console colors in places
AttrWindow ( -> ) - set attributes in the console window ( se