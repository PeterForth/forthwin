 needs toolset.f
 needs graphics.f
anew ffern.f


\ --------------------------------------------------------------------------
\ fern plotting
\ --------------------------------------------------------------------------

variable point_num
fvariable prob0
fvariable prob1
fvariable prob2
fvariable prob3
fvariable r
fvariable x
fvariable y
variable case_flag
fvariable p0
fvariable p1
fvariable p2
variable xtemp
variable ytemp


\ Here are the probabilities for the affine transformation in floating point
\  double prob[4] = { 0.85, 0.92, 0.99, 1.00 };

85e-2 prob0 f! 92e-2 prob1 f!  99e-2 prob2 f! 1e prob3 f!

\ 10000  point_num !  \ point_num is maximum numbers to plot


\   -----------------------------------------------
\ start of FractalFern
\ ------------------------------------------------
: FractalFern   \ start the routine
\  int i;
\  point2 p;
\  int point_num = 500000;

400000 point_num !  \ point_num=100000
\ calculate_points

\ Compute and plot the points.

\ get random numbers and convert them to floating

 100 random s>d d>f 100e f/ p0 f!  \  p0 = drand48 ( );

 100 random s>d d>f 100e f/ p1 f!  \ p1 = drand48 ( );


point_num @ 1- 0
   do     \   for ( i = 0; i < point_num; i++ )

    100 random s>d d>f 100e f/ r f! \ r = random number

     r f@ prob0 f@ f<    \ if ( r < prob[0] )
     if
       p0 f@ 85e-2 f* p1 f@ 4e-2 f*  f+ 0e1   f+   x f! \   x =   0.85 * p[0] + 0.04 * p[1] + 0.0;
       p0 f@ -4e-2 f* p1 f@ 85e-2 f* f+ 1.6e f+    y f! \   y = - 0.04 * p[0] + 0.85 * p[1] + 1.6;

     else
        r f@  prob1 f@ f<  \  if ( r < prob[1] )
        if
            p0 f@ 20e-2 f* p1 f@ -26e-2 f* f+ 0e   f+     x f! \   x =   0.20 * p[0] - 0.26 * p[1] + 0.0;
            p0 f@ 23e-2 f* p1 f@ 22e-2 f* f+ 1.6e f+     y f! \   y =   0.23 * p[0] + 0.22 * p[1] + 1.6;


        else
               r f@ prob2 f@ f<  \  if ( r < prob[2] )
               if
                  p0 f@ -15e-2 f* p1 f@ 28e-2 f* f+             x f! \    x = - 0.15 * p[0] + 0.28 * p[1] + 0.0;
                  p0 f@ 26e-2 f* p1 f@ 24e-2 f* f+  44e-2 f+    y f! \    y =   0.26 * p[0] + 0.24 * p[1] + 0.44;

               else
                 p0 f@ 0e1 f* p1 f@ 0e    f* 0e1 f+ 0e f+     x f! \    x =   0.00 * p[0] + 0.00 * p[1] + 0.0;
                 p0 f@ 0e f*  p1 f@ 16e-2 f* f+ 0e f+         y f! \    y =   0.00 * p[0] + 0.16 * p[1] + 0.0;
               endif
        endif
     endif



X f@ p0 f! \    p0 = x;
y f@ p1 f!  \    p1= y;


     p0 f@ 59e   f* f>d d>s 300 + ytemp !   \ multiply y times 50 to scale it upand shift it down and store it in ytemp
     p1 f@ 59e   f* f>d d>s   xtemp !   \ multiply x times 100 to scale it up and store it in xtemp
\  Plot the new point.


xtemp @ ytemp @

 cyan  (  red   rgb  )  set-dot

\

 loop
;

: demo            cls graphics-in-console



FractalFern
;
